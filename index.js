// console.log("Hello World");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

		function printWelcomeMessage(){
		let fullName = prompt("What is your name?");
		let age = prompt("How old are you?");
		let currentAddress = prompt("Where do you live?");

		console.log("Hello, " +fullName);
		console.log("You are " +age+ " years old.");
		console.log("You lived in " +currentAddress);
	}

	printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function top5Bands(){
		console.log("1. Mayonnaise");
		console.log("2. Hale");
		console.log("3. Cueshe");
		console.log("4. Rivermaya");
		console.log("5. Moonstar88");
	}

	top5Bands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function top5Movies(){
		console.log("1. The Hunger Games");
		console.log("Rotten Tomatoes Rating: 84%");
		console.log("2. The Addams Family");
		console.log("Rotten Tomatoes Rating: 65%");
		console.log("3. My Girl");
		console.log("Rotten Tomatoes Rating: 50%");
		console.log("4. Easy A");
		console.log("Rotten Tomatoes Rating: 85%");
		console.log("5. Set It Up");
		console.log("Rotten Tomatoes Rating: 92%");
	}

	top5Movies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");
	
	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();


// console.log(friend1);
// console.log(friend2);
